#OPTIONS=-O2 -Wall -std=c++11
# Décommentez la ligne suivante si vous désirez utiliser un débogueur.
OPTIONS = -g3 -ggdb -O0 -Wall -std=c++11
TEST_FILES = `ls [a-z]*-req[0-9]+.txt` rapport-*.txt log-*.txt

.PHONY: make_test

all : tp3 make_test

tp3 : tp3.cpp pointst.o carte.o fcarte.o
	g++ ${OPTIONS} -o $@ $^

pointst.o : pointst.cpp pointst.h
	g++ ${OPTIONS} -c -o $@ $<

carte.o : carte.cpp carte.h pointst.h
	g++ ${OPTIONS} -c -o $@ $<

fcarte.o : fcarte.cpp carte.h
	g++ ${OPTIONS} -c -o  $@ $<

clean: make_test
	rm -rf tp3 chemin *~ *.o *.gch *.swp
	-rm -f ${TEST_FILES}
	rm -rf results

check: all
	./tests/evaluer.sh
	mkdir -p results
	-mv ${TEST_FILES} results

make_test:
	$(MAKE) -C tests $(MAKECMDGOALS)
