/*  TP3
	Écrivez vos noms et codes permanents ici.
*/

#include "carte.h"
#include <queue>
#include <limits>

Carte::~Carte() {
	for (auto it = routes.begin(); it != routes.end(); ++it) {
		for (auto it2 = std::get<1>(*it)->begin(); it2 != std::get<1>(*it)->end(); ++it2) {
			delete *it2;
		}
		delete std::get<1>((*it));
	}
}

void Carte::ajouter_noeud(const long& id, const PointST& p) {
	noeuds_graphe[id] = p;
}

void Carte::ajouter_route(const std::string& nomroute, const std::list<long>& noeuds) {
	if (routes[nomroute] == NULL) {
		routes[nomroute] = new std::set<Edge*>();
	}
	long courant, prochain;
	for (auto it = noeuds.cbegin(); ;) {
		courant = *it;
		prochain = *(++it);
		if (it == noeuds.cend()) {
			break;
		}
		Edge* arete = new Edge(noeuds_graphe[courant], noeuds_graphe[prochain]);
		adjacences_sortantes[courant][prochain] = arete;
		adjacences_entrantes[prochain][courant] = arete;
		routes[nomroute]->insert(arete);
	}
}

void Carte::changerDisponibilites(const std::list<std::string>& nomsroute, const bool& estdisponible) {
	for (auto it = nomsroute.cbegin(); it != nomsroute.cend(); ++it) {
		for (auto it2 = (*routes[*it]).begin(); it2 != (*routes[*it]).end(); ++it2) {
			(*it2)->disponible = estdisponible;
		}
	}
}

long Carte::trouverDestIdeale(const long& noeudorigine, const double& distancecible, double& distancetrouve, std::list<long>& trajet) {
	std::unordered_map<long, NoeudDjikstra> noeuds_aller = std::unordered_map<long, NoeudDjikstra>();
	std::unordered_map<long, NoeudDjikstra> noeuds_retour = std::unordered_map<long, NoeudDjikstra>();
	//Djikstra
	// TODO: combiner les deux en un seul appel
	calculerDistances(noeudorigine, distancecible, adjacences_sortantes, noeuds_aller);
	calculerDistances(noeudorigine, distancecible, adjacences_entrantes, noeuds_retour);
	// TODO.
	double meilleure_distance = std::numeric_limits<double>::infinity();
	NoeudDjikstra chemin_aller, chemin_retour;
	for (auto iter = noeuds_aller.cbegin(); iter != noeuds_aller.cend(); ++iter) {
		long id_courant = std::get<0>(*iter);
		auto iter2 = noeuds_retour.find(id_courant);
		if(iter2 != noeuds_retour.end()) {
			NoeudDjikstra djik_aller = std::get<1>(*iter);
			NoeudDjikstra djik_retour = std::get<1>(*iter2);
			double total = djik_aller.distance_totale + djik_retour.distance_totale;
			if (total >= distancecible && total < meilleure_distance) {
				meilleure_distance = total;
				chemin_aller = djik_aller;
				chemin_retour = djik_retour;
			}
		}
	}
	distancetrouve = meilleure_distance;
	if (meilleure_distance == std::numeric_limits<double>::infinity()) {
			return -1;
	} else {
		for (long unsigned int i = 0; i < chemin_aller.parents.size(); i++) {
			trajet.push_back(chemin_aller.parents[i]);
		}
		trajet.push_back(chemin_aller.getId());
		if (chemin_retour.parents.size() > 0) {
			for (long unsigned int i = chemin_retour.parents.size() - 1; i > 0; i--) {
				trajet.push_back(chemin_retour.parents[i]);
			}
		}
		if (trajet.size() > 1) {
			trajet.push_back(noeudorigine);
		}
		return chemin_retour.getId();
	}
}

void Carte::calculerDistances(const long& noeud_origine, const double& distance_cible, std::unordered_map<long, std::unordered_map<long, Edge*>>& liste_adjacences, std::unordered_map<long, NoeudDjikstra>& noeuds_chemin) {
	std::priority_queue<NoeudDjikstra> a_visiter = std::priority_queue<NoeudDjikstra>();
	a_visiter.push(NoeudDjikstra(noeud_origine, 0));
	// boucle principale
	// TODO: ajouter meilleures conditions de fin (distance trop grande, distance infinie, etc.)
	// TODO: valider distance aller-retour vs selles déjà trouvées
	while(!a_visiter.empty()) {
		NoeudDjikstra courant = a_visiter.top();
		a_visiter.pop();
		if (noeuds_chemin.find(courant.getId()) != noeuds_chemin.cend()) {
			continue;
		// Si le trajet dans un sens dépasse la distance voulue, on arrête.
		/*} else if (courant.distance_totale > distance_cible) {
			break;*/
		}
		// noter la visite du noeud courant et le chemin à partir du départ.
		noeuds_chemin[courant.getId()] = courant;
		// ajouter tous les voisins à la file prioritaire
		for (auto iter = liste_adjacences[courant.getId()].cbegin(); iter != liste_adjacences[courant.getId()].cend(); ++iter) {
			long id_noeud = std::get<0>(*iter);
			Edge* edge_noeud = std::get<1>(*iter);
			if ((noeuds_chemin.find(id_noeud) == noeuds_chemin.cend()) && edge_noeud->disponible) {
				NoeudDjikstra noeud = NoeudDjikstra(id_noeud, courant.distance_totale + edge_noeud->distance);
				noeud.parents = courant.parents;
				noeud.parents.push_back(courant.getId());
				a_visiter.push(noeud);
			}
		}
	}
}

Carte::Edge::Edge() :disponible(true), distance() {}

Carte::Edge::Edge(const PointST& debut, const PointST& fin) :disponible(true), distance(debut.distance(fin)) {}

Carte::NoeudDjikstra::NoeudDjikstra() :parents(), distance_totale(std::numeric_limits<double>::infinity()), id() {}
Carte::NoeudDjikstra::NoeudDjikstra(const long& id) :parents(), distance_totale(std::numeric_limits<double>::infinity()), id(id) {}
Carte::NoeudDjikstra::NoeudDjikstra(const long& id, const double& distance) :parents(), distance_totale(distance), id(id) {}

bool Carte::NoeudDjikstra::operator<(const NoeudDjikstra& autre) const {
	return distance_totale > autre.distance_totale;
}

typename Carte::NoeudDjikstra Carte::NoeudDjikstra::operator=(const NoeudDjikstra& autre) {
	id = autre.id;
	distance_totale = autre.distance_totale;
	parents = autre.parents;
	return *this;
}
