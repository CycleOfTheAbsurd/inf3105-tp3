/*  TP3
	Écrivez vos noms et codes permanents ici.
*/

#if !defined(_CARTE__H_)
#define _CARTE__H_
#include <istream>
#include <list>
#include <vector>
#include <set>
#include <string>

#include <map>
#include <unordered_map>

#include "pointst.h"

class Carte {
  public:
	~Carte();
	void ajouter_noeud(const long& id, const PointST& p);
	void ajouter_route(const std::string& nom, const std::list<long>& noeuds);
	void changerDisponibilites(const std::list<std::string>& nomsroute, const bool& estdisponible);
	long trouverDestIdeale(const long& noeudorigine, const double& distancecible, double& distancetrouve, std::list<long>& trajet);

	// À compléter (vous pouvez tout changer).

	class Edge {
		public:
			Edge();
			Edge(const PointST& debut, const PointST& fin);
			bool disponible;
			double distance;
	};
	class NoeudDjikstra {
		public:
			NoeudDjikstra();
			NoeudDjikstra(const long& id);
			NoeudDjikstra(const long& id, const double& distance);
			bool operator<(const NoeudDjikstra& autre) const;
			NoeudDjikstra operator=(const NoeudDjikstra& autre);
			std::vector<long> parents;
			double distance_totale;
			long getId() const { return id; }
		private:
			long id;
	};
  private:
	// Résultats avec distances sont mis dans noeuds_chemin
	void calculerDistances(const long& noeud_origine, const double& distance_cible, std::unordered_map<long, std::unordered_map<long, Edge*>>& liste_adjacences, std::unordered_map<long, NoeudDjikstra>& noeuds_chemin);
	// À compléter.
	std::unordered_map<long, PointST> noeuds_graphe;
	std::unordered_map<std::string, std::set<Edge*>*> routes;
	// <id <id2, distance>>
	std::unordered_map<long, std::unordered_map<long, Edge*>> adjacences_sortantes;
	// <id2 <id, distance>>
	std::unordered_map<long, std::unordered_map<long, Edge*>> adjacences_entrantes;
};

// Déclaration opérateur (fonction) global
std::istream& operator >> (std::istream& is, Carte& carte);

#endif
